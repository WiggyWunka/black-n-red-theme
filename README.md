# The Black-N-Red iOS Theme
## by TheRealFabrizio

This is the Black-N-Red iOS theme created by TheRealFabrizio. Tested on iOS 5.1.1 on a 4th Generation iPod Touch.

For the color scheme used check misc/Base-Colors-Black-N-Red.txt

### Please handle with care